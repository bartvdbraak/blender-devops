# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

import buildbot.plugins

# Buildbot admin with access to everything.
admin_usernames = [
    "admin",
]

# Release engineers with access to store and deploy builders.
deploy_dev_usernames = [
    "admin",
]

# Trusted developers with access to trigger daily, doc and patch builds.
trusted_dev_usernames = [
    "admin",
]


def get_authentication(devops_env_id: str):
    class LocalEnvAuth(buildbot.plugins.util.CustomAuth):
        def check_credentials(self, user, password):
            return user.decode() == "admin" and password.decode() == "admin"

    return LocalEnvAuth()
